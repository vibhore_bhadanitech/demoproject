import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {freeApiService} from './services/freeapi.service';
import {comments} from './classes/comments'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private router: Router,
    private _freeApiServices: freeApiService
  ){}
  lstComments: comments[];
  ngOnInit(){
    // this._freeApiServices.getcomments()
    // .subscribe(
    //     data=>{
    //         this.lstComments=data;
    //     }
    // );
  }

  goToForm2(){
    this.router.navigate(['form2'])
  }
  userTable(){
    this.router.navigate(['userTable'])
  }
}
