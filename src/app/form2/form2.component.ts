import { Component, OnInit } from '@angular/core';
import { comments } from '../classes/comments';
import { freeApiService } from '../services/freeapi.service';

@Component({
  selector: 'app-form2',
  templateUrl: './form2.component.html',
  styleUrls: ['./form2.component.css']
})
export class Form2Component implements OnInit {

  constructor( private _freeApiServices: freeApiService) {}
  lstComments: comments[];
  ngOnInit(){
    this._freeApiServices.getcomments()
    .subscribe(
        data=>{
            this.lstComments=data;
        }
    );
  }

}
