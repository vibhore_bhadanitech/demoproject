import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Form1Component } from './form1/form1.component';
import { Form2Component } from './form2/form2.component';
import { UserTableComponent } from './user-table/user-table.component';


const routes: Routes = [
  {path: 'form1', component: Form1Component},
  {path: 'form1/:userId', component: Form1Component}, 
  {path: 'form2', component: Form2Component},
  {path : 'userTable' ,component: UserTableComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
