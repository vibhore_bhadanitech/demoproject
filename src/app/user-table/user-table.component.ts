import { Component, OnInit } from '@angular/core';
import { EVENT_MANAGER_PLUGINS } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {

  constructor(
    private route: Router
  ) { }
  userData = []
  startIndex =0;
  endIndex = 2;

  ngOnInit() {
    this.getUserData()
  }
  DeleteUser(i){
    this.userData.splice(i, 1);
    // localStorage.setItem('formvalue', JSON.stringify(this.userData))
  }
  editUser(id){
    this.route.navigate(['form1/' + id])
  }
  updateIndex(pageIndex)
  {
    this.startIndex = pageIndex *2;
    this.endIndex = this.startIndex +2;
  }

  getUserData(){
    if(localStorage.getItem('formvalue')){
      this.userData = JSON.parse(localStorage.getItem('formvalue'));
    }
  }
  searchUser(email){
    if(localStorage.getItem('formvalue')){
      let userList = JSON.parse(localStorage.getItem('formvalue'))
      let user = userList.find(x => x.email == email)
      console.log(user);
  }
}
getArrayFromData(length){
  return new Array(Math.ceil(length/2))

}
// filterUserDOB(){
//   if(localStorage.getItem('formvalue')){
//     this.userData = JSON.parse(localStorage.getItem('formvalue'));
//     for(let i =0;i<this.userData.length;i++)
//     {
//       (this.userData[i].dateOfBirth)
//     }
//   }
// }

  // searchUser(id){
  //   if(localStorage.getItem('formvalue')){
  //     let userList = JSON.parse(localStorage.getItem('formvalue'))
  //     let user = userList.find(x => x.userId == id)
  //     }
  //   }
  // 
}
